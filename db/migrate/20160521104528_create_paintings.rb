class CreatePaintings < ActiveRecord::Migration
  def change
    create_table :paintings do |t|
      t.string :name
      t.string :title
      t.string :description
      t.string :date
      t.string :collectie
      t.text :colors
      t.text :small_image
      
      t.timestamps null: false
    end
  end
end
