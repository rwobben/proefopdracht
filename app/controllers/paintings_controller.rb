class PaintingsController < ApplicationController
  
  def index
    @paintings = Painting.paginate(:page => params[:page], :per_page => 10)
  end
    
    
  def create
  end
    
  def show
    @painting = Painting.find(params[:id])
  rescue ActiveRecord::RecordNotFound
      flash[:alert] = 'Het schilderij dat u zoekt, kan niet gevonden worden'
      redirect_to paintings_path
  end
    
end

