class Painting < ActiveRecord::Base
  
  serialize :colors, JSON 
  serialize :small_image, JSON
  
end
