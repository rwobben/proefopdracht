Proefopdracht

Gemaakt door Roelof Wobben

*Proefopdracht* is gemaakt naar aanleiding van een proefopdracht van NoQxs om te laten zien of je ook programmer
ervaring hebt. De opdracht is om topstukken van het Rijksmuseum te laten zien, door middel van de API van het 
Rijksmuseum. 

## Installatie

Clone github repo 

```console
git clone git@bitbucket.org:rwobben/proefopdracht.git
```

Zorg dat alle plugins geinstalleerd worden

```console
cd  proefopdracht
bundle install
```

Zorg dat de database gemigreerd wordt. 

```console
rake db:migrate 
```


Zorg dat de database voor de eerste keer gevuld wordt
``` console 
rake read_database2 
```

## Testen 

Mocht u de testen willen draaien, doe dan deze stap 

``` console
rspec 
```

