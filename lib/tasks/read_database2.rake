desc 'read_database2'

task read_database2: :environment do
  
  # read all_data 
  url = "https://www.rijksmuseum.nl/api/nl/collection?key=14OGzuak&format=json&type=schilderij&toppieces=True"
  data = read_data(url)
  pages = data["count"] / 10
  all_numbers = [] 
  (0..pages).each do |i|
      url = "https://www.rijksmuseum.nl/api/nl/collection?key=14OGzuak&format=json&type=schilderij&toppieces=True&p=#{i}&ps=10"
      data = read_data(url)
      all_paintings = data["artObjects"]
      numbers = all_paintings.map { |item| item["objectNumber"] }
      all_numbers << numbers 
      numbers = [] 
  end
  
  all_numbers.flatten.map do  |number| 
    url = "https://www.rijksmuseum.nl/api/nl/collection/NUMBER?key=14OGzuak&format=json".gsub("NUMBER", number)
    data = read_data(url)
    
    url = "https://www.rijksmuseum.nl/api/nl/collection/NUMBER/tiles?key=14OGzuak&format=json".gsub("NUMBER", number)
    data2 = read_data(url)
    
    p number 
    
    Painting.create(
      title:         data["artObject"]["title"],
      description:   data["artObject"]["description"],
      date:          data["artObject"]["dating"]["year"],
      collectie:     data["artObject"]["objectCollection"].first,
      colors:        data["artObject"]["colors"],
      small_image:   data2["levels"].find{ |h| h["name"] == "z4" } )
    end  
  
    sleep(1)
end     
      
def read_data (url)
  response = HTTParty.get(url)
  return parsed_data = JSON.parse(response.body)
end

