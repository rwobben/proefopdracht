desc 'read_database'

task read_database: :environment do
  
  # read all_data 
  url = "https://www.rijksmuseum.nl/api/nl/collection?key=14OGzuak&format=json&type=schilderij&toppieces=True"
  data = read_data(url)
  all_paintings = data["artObjects"]
  all_numbers = all_paintings.map { |item| item["objectNumber"] }
        
  # lees alle gegevens
  all_numbers.map do  |number| 
    url = "https://www.rijksmuseum.nl/api/nl/collection/NUMBER?key=14OGzuak&format=json".gsub("NUMBER", number)
    data = read_data(url)
       
    # inlezen van de images 
      
    url = "https://www.rijksmuseum.nl/api/nl/collection/NUMBER/tiles?key=14OGzuak&format=json".gsub("NUMBER", number)
    data2 = read_data(url)
       
    Painting.find_or_initialize_by( name: data["artObject"]["makers"].first["name"]).update(
      title:         data["artObject"]["title"],
      description:  data["artObject"]["description"],
      date:        data["artObject"]["dating"]["year"],
      collectie:    data["artObject"]["objectCollection"].first,
      colors:     data["artObject"]["colors"],
      small_image:  data2["levels"].find{ |h| h["name"] == "z4" } )
  
  end        

end        
           
def read_data (url)
  response = HTTParty.get(url)
  return parsed_data = JSON.parse(response.body)
end


