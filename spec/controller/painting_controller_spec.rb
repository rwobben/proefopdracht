require 'rails_helper'

describe PaintingsController, type: :controller do
  describe 'handeling missing pages' do
    it 'handles a missing painting correctly' do
      get :show, id: 'not-here'
      expect(response).to redirect_to(paintings_path)
      message = 'Het schilderij dat u zoekt, kan niet gevonden worden'
      expect(flash[:alert]).to eq message
    end
  end
end
