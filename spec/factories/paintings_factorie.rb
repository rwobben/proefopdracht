FactoryGirl.define do
  factory :Painting do |f|
    f.name 'John'
    f.title 'Selfie'
    f.description 'stupid selfie'
    f.date '2012'
    f.collectie 'junk'
    f.colors ['white', 'yellow']
    f.small_image do
    { 'name'=>'z4', 'width'=>392, 'height'=>226, 'tiles'=>[{ 'x'=>0, 'y'=>0,
                                                             'url'=>'http://lh5.ggpht.com/8xOhbDTHqXZiRXjkKHvMpqFl3bS1liRV0mXMjrmA91FwhoDw1r2ZTfaZMgBw_ah-0mBj9V3XgeXMdXBEYc_hifsgxQ' }] }
    end
  end
end



