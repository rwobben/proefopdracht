require 'rails_helper'

RSpec.feature 'Users can visit the detail page' do
  
  scenario 'can_visit_the_index_page ' do
    
    project = FactoryGirl.create(:Painting)
        
    visit '/'
    click_link 'John'
    expect(page).to have_content 'John'
    expect(page).to have_content 'Selfie'
    expect(page).to have_content 'junk'
    expect(page).to have_content 'Datering'
    expect(page).to have_content 'Primaire kleuren'
    expect(page).to have_content 'Omschrijving'
  end
end
