require 'rails_helper'

RSpec.feature 'Users can visit the index page' do
  
  scenario 'can_visit_the_index_page ' do
        
    project = FactoryGirl.create(:Painting , name: 'paintings')
        
    visit '/'
    expect(page).to have_content 'Rijksmuseum'
    expect(page).to have_css('img')
  end
  
end